import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { AboutPage } from '../pages/about/about';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Config} from "../services/config";
import {SendToServerService} from "../services/send_to_server";
import {HttpModule} from "@angular/http";
import {PhonebookPage} from "../pages/phonebook/phonebook";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
      AboutPage,
      PhonebookPage,
  ],
  imports: [
      HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
      AboutPage,
      PhonebookPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
      Config,SendToServerService,

    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
