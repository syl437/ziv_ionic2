import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Config} from "../../services/config";

/**
 * Generated class for the PhonebookPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-phonebook',
  templateUrl: 'phonebook.html',
})
export class PhonebookPage implements OnInit{

  public PhoneBookArray:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public Settings:Config) {
  }
    ngOnInit()
    {
        this.PhoneBookArray = this.Settings.mainDataArray.phonebook;
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PhonebookPage');
  }

    DialPhone(phone)
    {
        window.open('tel:' + phone, '_system');
    }

}
