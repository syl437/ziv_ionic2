import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Config} from "../../services/config";

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage implements OnInit{

  public AboutData:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public Settings:Config) {
  }

    ngOnInit()
    {
      this.AboutData = this.Settings.mainDataArray.about;
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

    byContactLogin()
    {
      return false;
    }

}
