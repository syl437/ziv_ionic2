
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";


@Injectable()

export class SendToServerService
{
    public CategoryArray;


    constructor(private http:Http,public Settings:Config) { };

    getMainData(url:string)
    {
        let body = new FormData();
        //body.append('push_id', this.Settings.UserId.toString());
        return this.http.get(this.Settings.LaravelHost + '' + url, body).map(res => res.json()).do((data)=>{console.log("getCategories" , data), this.CategoryArray = data }).toPromise();
    }

};


